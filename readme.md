
# Create an AWS EKS Cluster | Node Group

## Technologies Used
- Kubernetes
- AWS EKS

## Project Description
- Configure necessary IAM Roles
- Create VPC with CloudFormation Template for Worker Nodes
- Create EKS cluster (Control Plane Nodes)
- Create Node Group for Worker Nodes and attach to EKS cluster
- Configure Auto-Scaling of worker nodes
- Deploy a sample application to EKS cluster

## Prerequisites
- AWS Account
- [Kubectl](https://kubernetes.io/docs/tasks/tools/)

## Guide Steps
### Create IAM Role for EKS
- Navigate to AWS Web UI > IAM > Roles > **Create Role**
	- Select trusted entity
		- Trusted entity type: **AWS service**
		- Use case: **EKS - Cluster**
		- **Next**
	- Add permissions
		- **Next**
	- Name, review, create
		- Role name: **eks-cluster-role**
	- **Create role**

### Create VPC for Worker Nodes
#### Create Stack
- Navigate to AWS Web UI > CloudFormation> **Create stack**
- [EKS VPC YAML Article](https://docs.aws.amazon.com/eks/latest/userguide/creating-a-vpc.html)
	- We'll use a combination of Public and Private Subnets IPv4 link
- **Next**

#### Specify Stack Details
- Stack Name: **eks-worker-node-vpc-stack**
- **Next**

#### Configure Stack Options
- Advanced settings we won't configure here
- **Next**

#### Review and Create
- **Submit**

### Create EKS Cluster
#### Configure Cluster
- Navigate to AWS Web UI > EKS > **Add cluster/create**
	- Name: **eks-cluster-test**
	- Kubernetes Version: **1.28**
	- Cluster service role: **eks-cluster-role**
	- Cluster Access: **Allow cluster administrator access**
		- **EKS API and ConfigMap**
	- Secrets Encryption: **Off**
	- **Next**

#### Specify Networking
- VPC: **eks-worker-node-vpc-stack**
- Security group: **eks-worker-node-vpc-stack**
- Cluster Endpoint Access: **Public and private**
- **Next**

#### Configure Observability
- **Next**

#### Select Add-ons
- Select Add-ons
	- CoreDNS
	- kube-proxy
	- Amazon VPC CNI
- **Next**

#### Configure Selected Add-ons Settings
- Accept default versions for all add-ons
- **Next**

#### Review and Create
- **Create**

### Connect to EKS locally with Kubectl
#### Create kubeconfig.yaml
- Verify current AWS configuration
	- `aws configure list`
- Update Kubeconfig, with cluster name
 `aws eks update-kubeconfig --name eks-cluster-test`
 - Verify connection to EKS cluster
	 - `kubectl cluster-info`
 
### Create Worker Nodes | Node Group
#### Create IAM Role for Node Group
- Navigate to AWS Web UI > IAM > Roles > **Create Role**
	- Select trusted entity
		- Trusted entity type: **AWS service**
		- Use case: **EC2**
		- **Next**
	- Add permissions
		- **AmazonEKSWorkerNodePolicy**
		- **AmazonEC2ContainerRegistryReadOnly**
		- **AmazonEKS_CNI_Policy**
		- **Next**
	- Name, review, create
		- Role name: **eks-node-group-role**
	- **Create role**
 
#### Add Node Group to EKS Cluster
- Navigate to AWS Web UI > EKS > **eks-cluster-test** > Compute > **Add node group**
	- Name: **eks-node-group**
	- Node IAM Role: **eks-node-group-role**
	- **Next**
- Set Compute and Scaling Configuration
	- AMI type: **Amazon Linux**
	- Capacity: **On-Demand**
	- Instance types: **t3.small**
	- Disk Size: **20 GiB**
	- Node Group Scaling Configuration
		- Default, **2** for each option
	- **Next**
- Specify Networking
	- Configure remote access to nodes: **Enabled**
		- Create a new `key pair` in order to SSH
		- Download `keypair.pem`
		- `chmod 400 keypair.pem`
		- `mv keypair.pem ~/.ssh/keypair.pem`
	- Allow remove access from **All**
	- **Next**
- Review and Create
	- **Create**
- Verify Nodes are Created (May take some time)
	- `kubectl get nodes`

### Configure Autoscaling in EKS Cluster
#### Create Auto Scaling Group
- We already have a group created by default when we made the EKS cluster. It's in the EC2 > Auto Scaling Groups Web UI section

#### Create Policy and Attach to Node Group IAM Role
- Navigate to AWS Web UI > IAM > Policies > **Create policy**
	- See `permission.json`file, copy/paste the contents into the JSON view
	- **Next**
	- Policy Name: **node-group-autoscale-policy**
		- **Create policy**
- Open our previously created **eks-node-group-role**
	- Add permissions > **Attach Policy**
	- Check off: **node-group-autoscaling-policy**
	- **Add Permissions**

#### Deploy K8s Autoscaler
- [Download the YAML for Autoscaler here](https://raw.githubusercontent.com/kubernetes/autoscaler/master/cluster-autoscaler/cloudprovider/aws/examples/cluster-autoscaler-autodiscover.yaml)  or run the command with the URL in the `-f`
	- `kubectl apply -f cluster-autoscaler-autodiscover.yaml`
- Verify Deployment
	- `kubectl get deployment -n kube-system cluster-autoscaler`
- Make Changes to the Deployment
	- Edit Configuration
		- `kubectl edit deployment -n kube-system cluster-autoscaler`
	- Add a new `annotation` line
		- **cluster-autoscaler.kubernetes.io/safe-to-evict: "false"**
	- Find the `<Your Cluster Name>` section and change it to **eks-cluster-test**
- Add two arguments to the command section
	- `- --balance-similar-node-groups`
	- `- --skip-nodes-with-system-pods=false`
- Edit the Image version of K8s to match what our cluster is using
	- I'm using version `1.28`, you can find yours in the AWS EKS Clusters menu
	- The minor revision number can be found here for the [Autoscaler Version Tags](https://github.com/kubernetes/autoscaler/tags)

![Added Annotation Line](/images/m11-1-add-annotation.png)

![Adding Extra Configuration Lines](/images/m11-1-extra-configuration-lines.png)

### Deploying Nginx and LoadBalancer
- Deploy Nginx
	- `kubectl apply -f nginx-config.yaml`

![Nginx Deployed](/images/m11-1-nginx-deployed.png)

![Nginx Accessible](/images/m11-1-nginx-accessible.png)